package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)

func initLog() *os.File {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.OpenFile(dir+"/logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	log.SetOutput(f)
	return f
}

func main() {
	fmt.Printf("%-15s : %-30s\n", "App name", AppName)
	fmt.Printf("%-15s : %-30s\n", "Version", Version)
	fmt.Printf("%-15s : %-30s\n", "Build time", BuildTime)
	fmt.Printf("%-15s : %-30s\n", "Build mode", BuildMode)

	file := initLog()
	defer file.Close()

	runBot()
}
