package main

import (
	"strconv"
	"strings"
	"sync"
	"time"
)

type BotConfig struct {
	LoadedAt      time.Time      `json:"-"`
	BotToken      string         `json:"token"`
	AllowedLogins []string       `json:"allowed_logins"`
	URLs          []CheckURL     `json:"urls"`
	Instructions  []*Instruction `json:"instructions"`
}

type Instruction struct {
	IsVisible bool     `json:"is_visible"`
	Name      string   `json:"name"`
	URLNames  []string `json:"url_names"`
	Repeat    *Repeat  `json:"repeat"`
}

type CheckURL struct {
	Name    string   `json:"name"`
	Method  string   `json:"method"`
	URL     string   `json:"url"`
	Headers []Header `json:"headers"`
	Body    string   `json:"body"`
}

type Header struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

type Repeat struct {
	IsInterval     bool
	RepeatInterval time.Duration
	Subscribe2xx   map[*User]struct{}
	Subscribe3xx   map[*User]struct{}
	Subscribe4xx   map[*User]struct{}
	Subscribe5xx   map[*User]struct{}

	SubscribeMutex *sync.RWMutex
}

func (repeat *Repeat) UnmarshalJSON(data []byte) error {

	dataString := strings.Trim(string(data), `"`)

	repeat.Subscribe2xx = make(map[*User]struct{})
	repeat.Subscribe3xx = make(map[*User]struct{})
	repeat.Subscribe4xx = make(map[*User]struct{})
	repeat.Subscribe5xx = make(map[*User]struct{})
	repeat.SubscribeMutex = &sync.RWMutex{}

	if strings.Contains(dataString, `:`) {
		repeat.IsInterval = false

		return nil
	} else {
		repeat.IsInterval = true
		seconds, err := strconv.ParseInt(dataString, 10, 64)

		repeat.RepeatInterval = time.Second * time.Duration(seconds)

		return err
	}
}

type User struct {
	Login  string
	ChatID int64
	Mutex  *sync.Mutex
}
