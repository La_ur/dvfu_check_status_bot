package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var userChats = make(map[string]*User)
var urls = make(map[string]*CheckURL)
var tgBot *tgbotapi.BotAPI
var instructionArray = make(map[string]*Instruction)

func runBot() {
	var msgText tgbotapi.MessageConfig
	var err error

	botConfig := loadConfig()

	for i := range botConfig.URLs {
		urls[botConfig.URLs[i].Name] = &botConfig.URLs[i]
	}

	tgBot, err = tgbotapi.NewBotAPI(botConfig.BotToken)
	if err != nil {
		log.Panic(err)
	}

	tgBot.Debug = isDebug

	fmt.Printf("Authorized on account %s\n", tgBot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := tgBot.GetUpdatesChan(u)

	for _, instruction := range botConfig.Instructions {
		instructionArray[instruction.Name] = instruction

		if instruction.Repeat.IsInterval {
			go repeatRequest(instruction, instruction.Repeat, &botConfig)
		}
	}

BOT_LOOP:
	for update := range updates {
		if update.Message == nil {
			continue BOT_LOOP
		}
		if update.Message.From.IsBot {
			continue BOT_LOOP
		}

		for _, login := range botConfig.AllowedLogins {
			if login == update.Message.From.UserName {

				if _, ok := userChats[login]; ok {
					userChats[login].ChatID = update.Message.Chat.ID
				} else {
					userChats[login] = &User{
						Login:  login,
						ChatID: update.Message.Chat.ID,
						Mutex:  &sync.Mutex{},
					}
				}

				go answerToUser(userChats[login], &update, &botConfig)

				continue BOT_LOOP
			}
		}

		msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Вы не имеете доступа к данному боту")

		_, err = tgBot.Send(msgText)
		if err != nil {
			log.Println(err)
			continue BOT_LOOP
		}
		continue BOT_LOOP
	}
}

func answerToUser(user *User, update *tgbotapi.Update, botConfig *BotConfig) {
	user.Mutex.Lock()
	defer user.Mutex.Unlock()

	var msgText tgbotapi.MessageConfig
	var err error

	for _, instruction := range botConfig.Instructions {
		if instruction.Name == update.Message.Text {

			runInstruction(user, instruction, createKeyboardForUser(user, botConfig))

			return
		}
	}

	if strings.HasPrefix(update.Message.Text, "Подписаться на ") {

		switch {
		case strings.HasSuffix(update.Message.Text, " 2xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 2xx"), "Подписаться на ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				instructionArray[instructionName].Repeat.Subscribe2xx[user] = struct{}{}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы подписались на "+instructionName+" 2xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}

			break
		case strings.HasSuffix(update.Message.Text, " 3xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 3xx"), "Подписаться на ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				instructionArray[instructionName].Repeat.Subscribe3xx[user] = struct{}{}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы подписались на "+instructionName+" 3xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}
			break
		case strings.HasSuffix(update.Message.Text, " 4xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 4xx"), "Подписаться на ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				instructionArray[instructionName].Repeat.Subscribe4xx[user] = struct{}{}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы подписались на "+instructionName+" 4xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}
			break
		case strings.HasSuffix(update.Message.Text, " 5xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 5xx"), "Подписаться на ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				instructionArray[instructionName].Repeat.Subscribe5xx[user] = struct{}{}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы подписались на "+instructionName+" 5xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}
			break
		}
	}

	if strings.HasPrefix(update.Message.Text, "Отписаться от ") {
		switch {
		case strings.HasSuffix(update.Message.Text, " 2xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 2xx"), "Отписаться от ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				if _, ok := instructionArray[instructionName].Repeat.Subscribe2xx[user]; ok {
					delete(instructionArray[instructionName].Repeat.Subscribe2xx, user)
				}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы отписались от "+instructionName+" 2xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}

			break
		case strings.HasSuffix(update.Message.Text, " 3xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 3xx"), "Отписаться от ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				if _, ok := instructionArray[instructionName].Repeat.Subscribe3xx[user]; ok {
					delete(instructionArray[instructionName].Repeat.Subscribe3xx, user)
				}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы отписались от "+instructionName+" 3xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}
			break
		case strings.HasSuffix(update.Message.Text, " 4xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 4xx"), "Отписаться от ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				if _, ok := instructionArray[instructionName].Repeat.Subscribe4xx[user]; ok {
					delete(instructionArray[instructionName].Repeat.Subscribe4xx, user)
				}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()

				msgText = tgbotapi.NewMessage(user.ChatID, "Вы отписались от "+instructionName+" 4xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}
			break
		case strings.HasSuffix(update.Message.Text, " 5xx"):

			instructionName := strings.TrimPrefix(strings.TrimSuffix(update.Message.Text, " 5xx"), "Отписаться от ")
			if _, ok := instructionArray[instructionName]; ok {
				instructionArray[instructionName].Repeat.SubscribeMutex.Lock()
				if _, ok := instructionArray[instructionName].Repeat.Subscribe5xx[user]; ok {
					delete(instructionArray[instructionName].Repeat.Subscribe5xx, user)
				}
				instructionArray[instructionName].Repeat.SubscribeMutex.Unlock()
				msgText = tgbotapi.NewMessage(user.ChatID, "Вы отписались от "+instructionName+" 5xx")
				msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

				_, err = tgBot.Send(msgText)
				if err != nil {
					log.Println(err)
				}
				return
			}
			break
		}
	}

	msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Доступ разрешен.\nВведите комманду")
	msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

	_, err = tgBot.Send(msgText)

	if err != nil {
		log.Println(err)
		return
	}
	return
}

func repeatRequest(instruction *Instruction, repeat *Repeat, botConfig *BotConfig) {

	t := time.NewTicker(repeat.RepeatInterval)
	for {
		<-t.C

		client := &http.Client{}
		for _, next := range instruction.URLNames {

			req, err := http.NewRequest(urls[next].Method, urls[next].URL, bytes.NewBuffer([]byte(urls[next].Body)))

			for _, header := range urls[next].Headers {
				req.Header.Set(header.Name, header.Content)
			}

			instructionStartTime := time.Now()
			resp, err := client.Do(req)
			seconds := time.Since(instructionStartTime)
			if err != nil {
				log.Println(err)
			}
			sendStatusMessagesToUsers(repeat, botConfig, seconds.Nanoseconds(), resp, next)

			resp.Body.Close()
		}
	}
}

func createKeyboardForUser(user *User, botConfig *BotConfig) *tgbotapi.ReplyKeyboardMarkup {
	keyboard := tgbotapi.NewReplyKeyboard()
	for _, instruction := range botConfig.Instructions {
		if instruction.IsVisible {
			keyboard.Keyboard = append(keyboard.Keyboard,
				tgbotapi.NewKeyboardButtonRow(
					tgbotapi.NewKeyboardButton(instruction.Name),
				))
		}
		if instruction.Repeat.IsInterval {

			instruction.Repeat.SubscribeMutex.RLock()

			if _, ok := instruction.Repeat.Subscribe2xx[user]; ok {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Отписаться от "+instruction.Name+" 2xx"),
					))
			} else {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Подписаться на "+instruction.Name+" 2xx"),
					))
			}

			if _, ok := instruction.Repeat.Subscribe3xx[user]; ok {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Отписаться от "+instruction.Name+" 3xx"),
					))
			} else {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Подписаться на "+instruction.Name+" 3xx"),
					))
			}

			if _, ok := instruction.Repeat.Subscribe4xx[user]; ok {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Отписаться от "+instruction.Name+" 4xx"),
					))
			} else {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Подписаться на "+instruction.Name+" 4xx"),
					))
			}

			if _, ok := instruction.Repeat.Subscribe5xx[user]; ok {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Отписаться от "+instruction.Name+" 5xx"),
					))
			} else {
				keyboard.Keyboard = append(keyboard.Keyboard,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("Подписаться на "+instruction.Name+" 5xx"),
					))
			}
			instruction.Repeat.SubscribeMutex.RUnlock()
		}
	}

	return &keyboard
}

func runInstruction(user *User, instruction *Instruction, keyboard *tgbotapi.ReplyKeyboardMarkup) {
	msgText := tgbotapi.NewMessage(user.ChatID, "Команда найдена. Начато выполнение")
	msgText.ReplyMarkup = keyboard
	_, err := tgBot.Send(msgText)
	if err != nil {
		log.Println(err)
		return
	}

	client := &http.Client{}

	for _, next := range instruction.URLNames {

		req, err := http.NewRequest(urls[next].Method, urls[next].URL, bytes.NewBuffer([]byte(urls[next].Body)))

		for _, header := range urls[next].Headers {
			req.Header.Set(header.Name, header.Content)
		}

		instructionStartTime := time.Now()
		resp, err := client.Do(req)
		seconds := time.Since(instructionStartTime)
		if err != nil {
			log.Println(err)

			msgText = tgbotapi.NewMessage(user.ChatID, "Ошибка: "+err.Error()+"\nВремя выполнения: "+strconv.FormatInt(seconds.Nanoseconds(), 10)+" нс.\n"+urls[next].URL)
			msgText.ReplyMarkup = keyboard

			_, err = tgBot.Send(msgText)
			if err != nil {
				log.Println(err)
				return
			}
			return
		}
		msgText := tgbotapi.NewMessage(user.ChatID, "Статус: "+resp.Status+"\nВремя выполнения: "+strconv.FormatInt(seconds.Nanoseconds(), 10)+" нс.\n"+urls[next].URL)
		msgText.ReplyMarkup = keyboard

		_, err = tgBot.Send(msgText)
		if err != nil {
			log.Println(err)
			return
		}

		resp.Body.Close()
	}

	msgText = tgbotapi.NewMessage(user.ChatID, "Выполнение команды завершено")
	msgText.ReplyMarkup = keyboard

	_, err = tgBot.Send(msgText)
	if err != nil {
		log.Println(err)
		return
	}
}

func sendStatusMessagesToUsers(repeat *Repeat, botConfig *BotConfig, requestTime int64, resp *http.Response, url string) {
	switch {
	case resp.StatusCode > 199 && resp.StatusCode < 300:

		repeat.SubscribeMutex.RLock()
		for user := range repeat.Subscribe2xx {
			msgText := tgbotapi.NewMessage(user.ChatID, "Статус: "+resp.Status+"\nВремя выполнения: "+strconv.FormatInt(requestTime, 10)+" нс.\n"+urls[url].URL)
			msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

			_, err := tgBot.Send(msgText)
			if err != nil {
				log.Println(err)
			}
		}
		repeat.SubscribeMutex.RUnlock()

		break
	case resp.StatusCode > 299 && resp.StatusCode < 400:

		repeat.SubscribeMutex.RLock()
		for user := range repeat.Subscribe2xx {
			msgText := tgbotapi.NewMessage(user.ChatID, "Ошибка: "+resp.Status+"\nВремя выполнения: "+strconv.FormatInt(requestTime, 10)+" нс.\n"+urls[url].URL)
			msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

			_, err := tgBot.Send(msgText)
			if err != nil {
				log.Println(err)
			}
		}
		repeat.SubscribeMutex.RUnlock()

		break
	case resp.StatusCode > 399 && resp.StatusCode < 500:

		repeat.SubscribeMutex.RLock()
		for user := range repeat.Subscribe2xx {
			msgText := tgbotapi.NewMessage(user.ChatID, "Ошибка: "+resp.Status+"\nВремя выполнения: "+strconv.FormatInt(requestTime, 10)+" нс.\n"+urls[url].URL)
			msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

			_, err := tgBot.Send(msgText)
			if err != nil {
				log.Println(err)
			}
		}
		repeat.SubscribeMutex.RUnlock()

		break
	case resp.StatusCode > 499 && resp.StatusCode < 600:

		repeat.SubscribeMutex.RLock()
		for user := range repeat.Subscribe2xx {
			msgText := tgbotapi.NewMessage(user.ChatID, "Ошибка: "+resp.Status+"\nВремя выполнения: "+strconv.FormatInt(requestTime, 10)+" нс.\n"+urls[url].URL)
			msgText.ReplyMarkup = createKeyboardForUser(user, botConfig)

			_, err := tgBot.Send(msgText)
			if err != nil {
				log.Println(err)
			}
		}
		repeat.SubscribeMutex.RUnlock()

		break
	}
}
