MKFILE_PATH =  $(patsubst %/,%,$(dir  $(abspath $(lastword $(MAKEFILE_LIST)))))

APP_NAME=DVFU Check Status Bot
APP_VERSION=1.1.5

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=dvfu_check_status_bot
BINARY_PATH=$(MKFILE_PATH)/bin
SOURCE_PATH=$(MKFILE_PATH)/src
BINARY_UNIX=$(BINARY_PATH)/linux/$(BINARY_NAME)_$(APP_VERSION)
BINARY_OSX=$(BINARY_PATH)/osx/$(BINARY_NAME)_$(APP_VERSION)

CURR_DATE=$(shell date -u +%d.%m.%Y)
CURR_TIME=$(shell date -u +%H:%M:%S)
CURR_DATE_TIME=$(CURR_DATE) $(CURR_TIME)

dep-ensure:
		cd $(MKFILE_PATH) && \
		dep ensure
prod-linux: dep-ensure
		cd $(SOURCE_PATH) && \
		CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -ldflags '-X "main.BuildTime=$(CURR_DATE_TIME)" -X "main.AppName=$(APP_NAME)" -X main.Version=$(APP_VERSION) -X main.BuildMode=production' -o $(BINARY_UNIX) -v
dev-linux: dep-ensure
		cd $(SOURCE_PATH) && \
		CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -ldflags '-X "main.BuildTime=$(CURR_DATE_TIME)" -X "main.AppName=$(APP_NAME)"  -X main.Version=$(APP_VERSION) -X main.BuildMode=debug' -o $(BINARY_UNIX) -v
prod-osx: dep-ensure
		cd $(SOURCE_PATH) && \
		CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -ldflags '-X "main.BuildTime=$(CURR_DATE_TIME)" -X "main.AppName=$(APP_NAME)" -X main.Version=$(APP_VERSION) -X main.BuildMode=production' -o $(BINARY_OSX) -v
dev-osx: dep-ensure
		cd $(SOURCE_PATH) && \
		CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 $(GOBUILD) -ldflags '-X "main.BuildTime=$(CURR_DATE_TIME)" -X "main.AppName=$(APP_NAME)"  -X main.Version=$(APP_VERSION) -X main.BuildMode=debug' -o $(BINARY_OSX) -v
run-linux:
		$(BINARY_UNIX)
run-osx:
		$(BINARY_OSX)