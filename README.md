# DVFU Check Status telegram bot  

## 1.1.5

## Deploy  

* [Install go-dep](https://github.com/golang/dep)  
* Run "dep install"  

## Build  

We use make to build project  

There are four commands:  

* dep-ensure  
    Run dep ensure  
    
* prod-linux  
    Build bin file for Linux to $(ProjectDir)/bin/linux/ in production mode 
    
* dev-linux  
    Build bin file for Linux to $(ProjectDir)/bin/linux/ in debug mode 
    
* prod-osx  
    Build bin file for OSX to $(ProjectDir)/bin/osx/ in production mode 

* dev-osx  
    Build bin file for OSX to $(ProjectDir)/bin/osx/ in debug mode 
    
* run-linux  
    Run bin from $(ProjectDir)/bin/linux/  

## Running  
Just run bin file  

**log is written in file "logfile" near bin file**  

You have to create file "bot_config.json" near bin file. **All fields are required. Arrays can be empty**
Cookies are saved during the execution of a instruction.
```json
{  
    "token":"TELEGRAM BOT TOKEN",  
    "allowed_logins":["LOGIN1","LOGIN2"],
    "urls":[
        {
            "name":"URL NAME AND DISPLAYED NAME",
            "method":"METHOD (GET|POST|PUT etc.) (choose one)",
            "url":"URL TO CHECK",
            "headers":[
                {
                    "name":"HEADER NAME",
                    "content":"HEADER CONTENT",
                }
            ],
            "body":"FOR METHODS THAT SUPPORT REQUEST BODY"
        }
    ],
    "instructions":[
        {
            "is_visible": true,
            "name": "DISPLAYED NAME",
            "url_names": ["URLNAME1","URLNAME2"],
            "repeat": "TIME_IN_SECONDS"
        }
    ]
}  
```  